package com.yu.constant;

import org.apache.commons.io.FileUtils;

import java.io.File;
import java.io.IOException;
import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

/**
 * @author yl
 */
public class AppProperties {
    /**
     * 数据文件夹
     */
    private static final String DATA_DIRECTORY = "DATA";
    /**
     * 临时文件夹
     */
    private static final String DATA_FILE_DIRECTORY = "File";

    /**
     *  word 文件类型
     */
    private static final String [] WORD_TYPE = {"doc","docx","wps"};

    private static final Logger L = LogManager.getLogger();

    private static final AppProperties OUR_INSTANCE = new AppProperties();

    private File dataDirectory;
    private File dataFileDirectory;

    private String [] wordType;



    private AppProperties() {
        try {
            //数据目录
            dataDirectory = new File("/", DATA_DIRECTORY);
            if (!dataDirectory.exists()) {
                dataDirectory = new File(DATA_DIRECTORY);
            }
            if (!dataDirectory.exists()) {
                FileUtils.forceMkdir(dataDirectory);
            }
            L.debug("目录:{}", dataDirectory.getAbsolutePath());
            //文件目录
            dataFileDirectory = new File(dataDirectory, DATA_FILE_DIRECTORY);
            FileUtils.forceMkdir(dataFileDirectory);
            L.debug("文件目录:{}", dataFileDirectory.getAbsolutePath());
        } catch (IOException e) {
            L.catching(e);
        }
    }

    public static AppProperties getInstance() {
        return OUR_INSTANCE;
    }

    public File getDataDirectory() {
        return dataDirectory;
    }

    public File getDataFileDirectory() {
        return dataFileDirectory;
    }

    public String[] getWordType() {
        return WORD_TYPE;
    }
}
