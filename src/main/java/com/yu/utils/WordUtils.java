package com.yu.utils;


import cn.hutool.core.io.FileUtil;
import com.aspose.words.Document;
import com.aspose.words.License;
import com.yu.constant.AppProperties;
import org.apache.commons.lang.StringUtils;


import java.io.*;

import static com.aspose.words.LoadFormat.HTML;
import static com.yu.utils.HtmlUtils.htmlImgToBase64;


/**
 * @author yl
 */
public class WordUtils {

    /**
     *  加载 aspose 去水印 license
     */
    private static void init()  {
        try {
            InputStream is = WordUtils.class.getClassLoader().getResourceAsStream("license.xml");
            License license = new License();
            license.setLicense(is);
        } catch (Exception e) {
            e.printStackTrace();
        }

    }

    /**
     *  word 转换 html 文件
     *
     * @param wordFile 图片文件
     * @param htmlFilePath html 文件保存位置
     */
    public static void wordToHtml(File wordFile,String htmlFilePath){
        if (!checkWordType(wordFile)){
            System.out.println("不支持此文件类型转换");
            return;
        }
        Document doc;
        try {
            init();
            doc = new Document(wordFile.getPath());
            doc.save(htmlFilePath, HTML);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    /**
     *  word 转换 html 文件
     *
     * @param wordFile 图片文件
     * @param htmlFilePath html 文件保存位置
     * @return html 文件
     */
    public static File wordToHtmlFile(File wordFile,String htmlFilePath) throws Exception {
        wordToHtml(wordFile,htmlFilePath);
        File htmlFile = new File(htmlFilePath);
        String htmlCode = htmlImgToBase64(htmlFile);
        writeHtml(htmlCode,htmlFilePath);
        return htmlFile;
    }

    /**
     *  word 转换 html 文件
     *
     * @param wordFile 图片文件
     * @param htmlFilePath html 文件保存位置
     * @return html 文本
     */
    public  static  String wordToHtmlCode(File wordFile,String htmlFilePath){
        wordToHtml(wordFile,htmlFilePath);
        return htmlImgToBase64(new File(htmlFilePath));
    }



    /**
     *  检查文件是否文档类型
     * @return boolean
     */
    public static boolean checkWordType(File file){
        String fileType = FileUtil.getSuffix(file);
        for (String s : AppProperties.getInstance().getWordType()) {
            if (StringUtils.equalsIgnoreCase(fileType,s)){
                return true;
            }
        }
        return false;
    }




    /**
     * 转换后的 html文本 回写文件
     *
     * @param htmlCode html文本
     * @param htmlFilePath html文件位置
     */
    public static void writeHtml(String htmlCode, String htmlFilePath) throws Exception {
        FileOutputStream fos = null;
        OutputStreamWriter osw = null;
        try {
            File htmlFile=new File(htmlFilePath);
            if (!htmlFile.getParentFile().exists()) {
                htmlFile.getParentFile().mkdirs();
            }
            fos = new FileOutputStream(htmlFile);
            osw = new OutputStreamWriter(fos, "UTF-8");
            osw.write(htmlCode);
            osw.flush();
        } finally {
            if (osw != null) {
                osw.close();
            }
            if (fos != null) {
                fos.close();
            }
        }
    }


}
