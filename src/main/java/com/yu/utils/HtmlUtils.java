package com.yu.utils;

import cn.hutool.core.io.FileUtil;
import com.yu.constant.AppProperties;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.File;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import static com.yu.utils.ImageUtils.imageToBase64Head;



/**
 * @author yl
 */
public class HtmlUtils {

    private static final Pattern BA64_PATTERN = Pattern.compile("^data:image/(png|gif|jpg|jpeg|bmp|tif|psd|ICO);base64,.*");

    private static final Pattern IMG_PATTERN = Pattern.compile("^.*[.](png|gif|jpg|jpeg|bmp|tif|psd|ICO)$");

    /**
     *  解析 html文件将其中的 image 图片转 BASE64 编码
     *
     * @param html html文件
     * @return 编码替换后的html文本
     */
    public static String htmlImgToBase64(File html) {
        org.jsoup.nodes.Document doc = null;
        try {
            doc= Jsoup.parse(html,"UTF-8");
        }catch (Exception e){
            e.printStackTrace();
        }
        assert doc != null;
        Elements image = doc.getElementsByTag("img");
        for (Element img : image) {
            String src = img.attr("src");
            String imgSrc = new File(AppProperties.getInstance().getDataFileDirectory(), src).getPath();
            // 检查
            if (checkImage(imgSrc)) {
                // 或者图片的 base64 编码
                String imgBase64 = imageToBase64Head(imgSrc);
                // 回写 src
                img.attr("src", imgBase64);
            }
        }
        // 返回编码后的 html 文档
        return doc.html();
    }

    /**
     *  解析 html文件将其中的 image 图片转 BASE64 编码
     *
     * @param src html 中图片位置
     * @return 为图片返回 ture
     */
    public static boolean checkImage(String src) {
        // 排除图片已经是 BASE64 格式的情况
        Matcher matcher = BA64_PATTERN.matcher(src);
        if (matcher.matches()) {
            return false;
        }
        // 排除src路径并非图片格式的情况
        matcher = IMG_PATTERN.matcher(src);
        if (!matcher.matches()) {
            return false;
        }
        // 排除图片路径不存在的情况
        File file = new File(src);
        return file.exists();
    }


}
