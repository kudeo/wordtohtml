package com.yu.utils;

import cn.hutool.core.io.FileUtil;

import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

/**
 * @author yl
 */
public class ImageUtils {
    /**
     *  image 图片转 BASE64 编码
     *
     * @param imgFilePath 图片文件位置
     * @return 图片的base64编码
     */
    public static String imgChangeBase64(String imgFilePath) {
        //将图片文件转化为字节数组字符串，并对其进行Base64编码处理
        InputStream in = null;
        byte[] data = null;
        //读取图片字节数组
        try {
            in = new FileInputStream(imgFilePath);
            data = new byte[in.available()];
            in.read(data);
            in.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return new String(java.util.Base64.getEncoder().encode(data));
    }

    /**
     *  添加 BASE64 头文件
     * @return BASE64 头文件
     */
    public static String imageToBase64Head(String imgFile) {
        //为编码添加头文件字符串
        String type = FileUtil.getSuffix(imgFile);
        String head = "data:image/" + type + ";base64,";
        return head + imgChangeBase64(imgFile);
    }
}
