package com.yu;

import com.yu.constant.AppProperties;
import com.yu.utils.WordUtils;

import java.io.File;

public class Main {
    public static void main(String[] args) throws Exception {

        String wordFile = "E:\\my\\code\\testResource\\文档.doc";
        String htmlFile =  new File(AppProperties.getInstance().getDataFileDirectory(), "test.html").getPath();
        // 可直接转换成 html 不替换 图片
//        WordUtils.wordToHtml(new File(wordFile), htmlFile);
//         替换图片，返回文件
        File file = WordUtils.wordToHtmlFile(new File(wordFile), htmlFile);
        // 替换图片，返回文本
        //String s = WordUtils.wordToHtmlCode(new File(wordFile), htmlFile);
        //System.out.println(s);
    }
}