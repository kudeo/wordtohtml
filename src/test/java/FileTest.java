import cn.hutool.core.io.FileUtil;
import com.aspose.words.License;
import org.apache.commons.lang.StringUtils;
import org.junit.jupiter.api.Test;
import com.aspose.words.Document;
import com.aspose.words.SaveFormat;

import java.io.InputStream;

public class FileTest {
    @Test
    public void toHtmlTest() throws Exception {
        String sourceFileRoot = "E:\\my\\code\\testResource\\";
        String htmlPathRoot = "E:\\my\\code\\testResource\\html\\";

        String sourceFileName = "文档.wps";
        String htmlFileName = "out.html";

        String sourceFilePath = sourceFileRoot + sourceFileName;
        String htmlFilePath = htmlPathRoot + htmlFileName;

        String fileType = sourceFileName.substring(sourceFileName.lastIndexOf(".") + 1).toLowerCase();
        init();
        if (StringUtils.startsWith(fileType, "doc") || StringUtils.startsWith(fileType, "docx")|| StringUtils.startsWith(fileType, "wps")) {
            Document doc = new Document(sourceFilePath);
            doc.save(htmlFilePath, SaveFormat.HTML);
        }
        byte[] htmlBytes = readBytes(htmlFilePath);
        // 删除临时word文件
//        deleteFileOrFolder(htmlFilePath);
    }

    public void init() throws Exception {
        InputStream is = getClass().getResourceAsStream("license.xml");
        License license = new License();
        license.setLicense(is);
    }

    /**
     * 根据路径删除指定的目录或文件，无论存在与否
     *
     * @param fullFileOrDirPath: 要删除的目录或文件
     * @return 删除成功返回 true，否则返回 false
     * @author zhengqing
     * @date 2020/9/5 20:56
     */
    public static boolean deleteFileOrFolder(String fullFileOrDirPath) {
        return FileUtil.del(fullFileOrDirPath);
    }

    /**
     * 读取文件数据
     *
     * @param filePath: 文件路径
     * @return 文件字节码
     * @author zhengqing
     * @date 2020/9/5 23:00
     */
    public static byte[] readBytes(String filePath) {
        return FileUtil.readBytes(filePath);
    }



}
